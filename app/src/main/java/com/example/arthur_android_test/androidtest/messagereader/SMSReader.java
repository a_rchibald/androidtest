package com.example.arthur_android_test.androidtest.messagereader;

import android.database.Cursor;
import android.net.Uri;
import android.widget.Toast;

import com.example.arthur_android_test.androidtest.R;
import com.example.arthur_android_test.androidtest.activities.MainActivity;
import com.example.arthur_android_test.androidtest.models.SMSModel;

import java.util.ArrayList;

/**
 * Created by Artur on 05.05.2015.
 */
public class SMSReader {

    private final static String URI = "content://sms/inbox";
    private final static String ID = "_id";
    private final static String ADDRESS = "address";
    private final static String BODY = "body";

    public ArrayList<SMSModel> getSMSList(MainActivity activity){
        ArrayList<SMSModel> modelList = new ArrayList<>();
        Uri uri = Uri.parse(URI);
        String[] columns = new String[] {ID, ADDRESS, BODY};
        Cursor cursor = activity.getContentResolver().query(uri, columns, null, null, null);
        if(cursor != null) {
            if (cursor.moveToFirst()){
                for(int i = 0; i<cursor.getCount(); i++) {
                    String message = cursor.getString(2);
                    String from = cursor.getString(1);
                    modelList.add(new SMSModel(message, from));
                    cursor.moveToNext();
                }
            }
            cursor.close();
        } else
            Toast.makeText(activity, activity.getResources().getString(R.string.no_data), Toast.LENGTH_LONG).show();
        return modelList;
    }
}
