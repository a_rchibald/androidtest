package com.example.arthur_android_test.androidtest.httprequests;

import android.content.Context;
import android.text.TextUtils;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.arthur_android_test.androidtest.interfaces.IProgressDialog;
import com.example.arthur_android_test.androidtest.R;
import com.example.arthur_android_test.androidtest.adapters.MessageAdapter;

/**
 * Created by Artur on 06.05.2015.
 */
public class ControllerRequest {

    private final static String WEB_ADDRESS = "http://app.mobilenobo.com/c/apptest?id=";
    private MessageAdapter adapter;

    public ControllerRequest(MessageAdapter adapter) {
        this.adapter = adapter;
    }

    public void makeRequest(final Context context, final IProgressDialog progressDialog) {
        progressDialog.showProgressDialog();
        RequestQueue queue = Volley.newRequestQueue(context);
        String api = WEB_ADDRESS+adapter.getSMSModel().getTailString();
        StringRequest stringRequest = new StringRequest(Request.Method.GET, api,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if(!TextUtils.isEmpty(response)) {
                            Toast.makeText(context, response, Toast.LENGTH_LONG).show();
                            progressDialog.dismissProgressDialog();
                        }
                        else
                            Toast.makeText(context, context.getResources().getString(R.string.no_data_from_server), Toast.LENGTH_LONG).show();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(context, error.toString(), Toast.LENGTH_LONG).show();
            }
        });
        queue.add(stringRequest);
    }
}
