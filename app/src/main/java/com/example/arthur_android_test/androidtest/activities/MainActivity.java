package com.example.arthur_android_test.androidtest.activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.example.arthur_android_test.androidtest.interfaces.IProgressDialog;
import com.example.arthur_android_test.androidtest.R;
import com.example.arthur_android_test.androidtest.adapters.MessageAdapter;
import com.example.arthur_android_test.androidtest.httprequests.ControllerRequest;
import com.example.arthur_android_test.androidtest.messagereader.SMSReader;
import com.example.arthur_android_test.androidtest.models.SMSModel;

import java.util.ArrayList;


public class MainActivity extends Activity implements View.OnClickListener, IProgressDialog{

    private final static String LINK = "apptest.com/i?id=";
    private MessageAdapter adapter;
    private ProgressDialog pDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        findViewById(R.id.start_btn).setOnClickListener(this);
        pDialog = new ProgressDialog(this);
    }

    @Override
    public void onClick(View view) {
        findSpecificLine(new SMSReader().getSMSList(this));
    }

    private void findSpecificLine(ArrayList<SMSModel> arrayList){
        createMessagesList(arrayList);
        setAdapter(this);
    }

    private void createMessagesList(ArrayList<SMSModel> arrayList){
        ArrayList<SMSModel> smsModels = new ArrayList<>();
        for(SMSModel smsModel : arrayList){
            String messageFromModel = smsModel.getMessageBody();
            if(messageFromModel.contains(LINK)){
                String result = subStrString(smsModel.getMessageBody());
                if(result.contains(LINK)) {
                    if (!result.substring(18).matches("^.*[^a-zA-Z0-9 ].*$")) {
                        String tailString = result.substring(17);
                        smsModels.add(new SMSModel(result, smsModel.getFrom(), tailString));
                    }
                }
            }
        }
        if(smsModels.size() == 0)
            Toast.makeText(this, getResources().getString(R.string.not_found), Toast.LENGTH_SHORT).show();
        else
            adapter = new MessageAdapter(getLayoutInflater(), smsModels);
    }

    private void setAdapter(final IProgressDialog progressDialog){
        final ControllerRequest controller = new ControllerRequest(adapter);
        ListView listView = (ListView) findViewById(R.id.messages_list_id);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                controller.makeRequest(getBaseContext(), progressDialog);
            }
        });
    }

    private String subStrString(String origin){
        String start = origin.substring(origin.indexOf(LINK));
        String end;
        if(start.contains(" "))
            end = start.substring(start.indexOf(LINK), start.indexOf(" "));
        else
            end = start.substring(start.indexOf(LINK));
        return end;
    }

    @Override
    public void showProgressDialog() {
        if(!pDialog.isShowing())
            pDialog.show();
    }

    @Override
    public void dismissProgressDialog() {
        if(pDialog.isShowing())
            pDialog.dismiss();
    }
}
