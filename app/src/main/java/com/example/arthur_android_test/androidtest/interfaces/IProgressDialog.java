package com.example.arthur_android_test.androidtest.interfaces;

/**
 * Created by Artur on 09.05.2015.
 */
public interface IProgressDialog {
    void showProgressDialog();
    void dismissProgressDialog();
}
