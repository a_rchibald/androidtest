package com.example.arthur_android_test.androidtest.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.arthur_android_test.androidtest.R;
import com.example.arthur_android_test.androidtest.models.SMSModel;

import java.util.ArrayList;

/**
 * Created by Artur on 06.05.2015.
 */
public class MessageAdapter extends BaseAdapter {

    private ArrayList<SMSModel> arrayList;
    private LayoutInflater inflater;
    private SMSModel model;

    public MessageAdapter(LayoutInflater inflater, ArrayList<SMSModel> arrayList){
        this.arrayList = arrayList;
        this.inflater = inflater;
    }

    @Override
    public int getCount() {
        return arrayList.size();
    }

    @Override
    public SMSModel getItem(int position) {
        return arrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View view = convertView;

        if(view == null){
            view = inflater.inflate(R.layout.list_view_item, parent, false);
            ViewHolder holder = new ViewHolder();
            holder.messageBody = (TextView) view.findViewById(R.id.message_body_id);
            holder.messageAddress = (TextView) view.findViewById(R.id.message_address_id);
            view.setTag(holder);
        }

        model = getItem(position);

        ViewHolder viewHolder = (ViewHolder) view.getTag();
        viewHolder.messageAddress.setText(view.getResources().getString(R.string.from) + " " + model.getFrom());
        viewHolder.messageBody.setText(view.getResources().getString(R.string.message) + " "
                + view.getResources().getString(R.string.http)
                + model.getMessageBody());

        return view;
    }

    private static class ViewHolder {
        TextView messageBody;
        TextView messageAddress;
    }

    public SMSModel getSMSModel(){
        return model;
    }
}
