package com.example.arthur_android_test.androidtest.models;

/**
 * Created by Artur on 05.05.2015.
 */
public class SMSModel {

    private String messageBody;
    private String from;
    private String tailString;

    public SMSModel(String messageBody, String from) {
        this.messageBody = messageBody;
        this.from = from;
    }

    public SMSModel(String messageBody, String from, String tailString) {
        this.messageBody = messageBody;
        this.from = from;
        this.tailString = tailString;
    }

    public String getMessageBody() {
        return messageBody;
    }

    public void setMessageBody(String messageBody) {
        this.messageBody = messageBody;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTailString() {
        return tailString;
    }

    public void setTailString(String tailString) {
        this.tailString = tailString;
    }
}
